debos (1.0.0+git20191223.292995b-1) unstable; urgency=medium

  * New upstream snapshot 1.0.0+git20191223.292995b.

 -- Andrej Shadura <andrewsh@debian.org>  Thu, 16 Jan 2020 16:50:56 +0100

debos (1.0.0+git20190906.f5be960-1) unstable; urgency=medium

  * New upstream snapshot 1.0.0+git20190906.f5be960.

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 09 Sep 2019 07:01:02 +0000

debos (1.0.0+git20190326.5bd4aa9-2) unstable; urgency=medium

  * Upload to unstable.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 12 Jul 2019 00:28:17 +0200

debos (1.0.0+git20190326.5bd4aa9-1) experimental; urgency=medium

  * New upstream snapshot 1.0.0+git20190326.5bd4aa9.

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 26 Mar 2019 16:07:10 +0100

debos (1.0.0+git20190319.cf3fc48-2) experimental; urgency=medium

  * Bump the build dependency on golang-github-sjoerdsimons-ostree-go-dev.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 25 Mar 2019 12:17:58 +0100

debos (1.0.0+git20190319.cf3fc48-1) experimental; urgency=medium

  * Upload to experimental.
  * New upstream version 1.0.0+git20190319.cf3fc48.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 25 Mar 2019 10:34:19 +0100

debos (1.0.0+git20190123.d6e16be-1) unstable; urgency=medium

  * New upstream snapshot 1.0.0+git20190123.d6e16be.
  * Watch the git repo.
  * Add myself to Uploaders.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 08 Feb 2019 16:33:20 +0100

debos (1.0.0+git20181105.b02e058-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Do not install sources.

  [ Héctor Orón Martínez ]
  * New upstream version 1.0.0+git20181105.b02e058
  * debian/gbp.conf: switch to DEP14
  * debian/control: set rules require no root
  * debian/control: add new build dependency on go-losetup.v1
  * debian/control: version fakemachine dependency (Closes: #905923)
  * debian/README.source: add new file (Closes: #891723)
  * debian/manpages: ship manpage (Closes: #893473)

 -- Héctor Orón Martínez <zumbi@debian.org>  Fri, 09 Nov 2018 10:45:47 +0100

debos (1.0.0+git20180808.5b74d5d-1) unstable; urgency=medium

  * New upstream version 1.0.0+git20180808.5b74d5d
  * debian/control: bump standards version

 -- Héctor Orón Martínez <zumbi@debian.org>  Fri, 10 Aug 2018 18:14:06 +0200

debos (1.0.0+git20180328.8f2bc2a-1) unstable; urgency=medium

  [ Geert Stappers ]
  * debian/control: recommend busybox (Closes: #891714)

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Ana Guerrero Lopez ]
  * Update Depends:
    - add qemu-system and qemu-user-static
    - add debootstrap
  * Update Recommends:
    - Add gzip, bzip2, xz, tar, zip that are often used by debos
    - Add binaries needed by the actions in image-partition:
      util-linux, parted, mount, udev
  * Small description update

  [ Héctor Orón Martínez ]
  * New upstream version 1.0.0+git20180328.8f2bc2a
  * debian/control: restrict to arch amd64
  * debian/control: bump standars version
  * debian/control: rework depends and recommends

 -- Héctor Orón Martínez <zumbi@debian.org>  Sat, 19 May 2018 16:33:01 +0200

debos (1.0.0+git20180112.6e577d4-1) unstable; urgency=medium

  * Initial release (Closes: #886772)

 -- Héctor Orón Martínez <zumbi@debian.org>  Tue, 20 Feb 2018 16:48:59 +0100
